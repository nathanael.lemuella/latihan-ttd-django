// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = '';
    erase = false;
  } 

  else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;    
  }

  else if (x === 'log' || x ==='sin' || x === 'tan') {
    switch (x) {
      case 'log':
        print.value = Math.log10(print.value);
        erase = true;
        break;
      case 'sin':
        print.value = Math.sin(print.value);
        erase = true;
        break;
      case 'tan':
        print.value = Math.sin(print.value);
        erase = true;
        break;
    }
  }
  
  else {
    print.value += x;
    erase = true;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

//Themes 
$(document).ready(function() {
  var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
  ] ; 
  
  
  var items = JSON.stringify(themes);
  localStorage.setItem("items", items) ; 
  var mySelect = $('.my-select').select2();

  mySelect.select2({
    'data': JSON.parse(localStorage.getItem("items")) 
  });

  var arrayOfThemes = JSON.parse(localStorage.getItem("items")) ; 
  var default_themes = arrayOfThemes[3] ; 
  var select = default_themes ; 
  
  if (localStorage.getItem("select") !== null){
    select =  JSON.parse(localStorage.getItem("select"));
 }
     $('body').css(
      {
      "background-color": select.bcgColor , 
      "font-color"      : select.fontColor
      }
    );

  $('.apply-button').on('click', function(){
    var retrievedObject = mySelect.val() ;
    select = arrayOfThemes[retrievedObject] ;
    $('body').css(
      {
      "background-color": select.bcgColor , 
      "font-color"      : select.fontColor
      }
    );
    localStorage.setItem("select", JSON.stringify(select)) ; 
  })

  //ChatBox
  var cls = "msg-receive";
  $('#text').keypress(function (e) {
      if (e.which == 13) {
          var msg = $('#text').val();
          var old = $('.msg-insert').html();
          if (msg.length === 0) {
              alert("Message kosong");
          }
          else {
              cls = (cls == "msg-send") ? "msg-receive" : "msg-send"
              $('.msg-insert').html(old + '<p class=' + cls + '>' + msg + '</p>')
              $('#text').val("");
          }
      }
  });

  $('.msg-insert').keyup(function (e) {
      if (e.which == 13) {
          $('#text').val("");
      }
  });  
});


