from django.db import models
from django.utils import timezone
import pytz


def convertTime():
	    return timezone.now() + timezone.timedelta(hours=7)

class Message(models.Model):
    name = models.CharField(max_length=27)
    email = models.EmailField()
    message = models.TextField()
    created_date = models.DateTimeField(default=convertTime)

    def __str__(self):
        return self.message
